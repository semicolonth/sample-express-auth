const express = require('express')

const app = express()

const listenIP = '127.0.0.1'
const listenPort = 8088

app.use('/public', express.static('public'))

app.use((req, res, next) => {
    let token = ''
    if (req.header('Authorization')) {
        token = req.header('Authorization').match(/^Bearer\s+(\S+)$/)[1];
    }
    console.log(`Detected Token: ${token}`);
    if(token == 'abc') {
        req.context = {
            token
        }
        next()
    } else {
      res.sendStatus(403);
    }
})

app.use('/protected', express.static('protected'))

app.use('/protected', (req, res) => {
    res.status(200).json({
        message: `In protected area with token = ${req.context.token}`
    })
})

app.listen(listenPort, listenIP, function () {
    console.log('Server Listening on ' + listenIP + ':' + listenPort)
})
